require "dokker/version"
require "dokker/client"
require "dokker/claims"

module Dokker
  class << self 
    attr_accessor :configuration

    def configure 
      self.configuration ||= Configuration.new 
      yield(configuration)
    end

    def reset
      self.configuration = Configuration.new
    end
  end

  class Configuration 
    attr_accessor :url, :api_key, :api_secret
  end

  class Error < StandardError; end

end
