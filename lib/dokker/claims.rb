module Dokker 
  module Claims 
    def self.call(args)
      args.merge(standard_claims)      
    end
    
    private 

    def self.standard_claims
      { 
        jti: SecureRandom.uuid, 
        iat: Time.now.to_i,
        exp: Time.now.to_i + (60 * 60 * 1)
      }
    end
  end
end