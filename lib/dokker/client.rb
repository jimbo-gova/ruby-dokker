require 'jwt'
require 'securerandom'
require 'uri'

module Dokker 
  class Client 
    attr_reader :url, :api_key, :api_secret, :credentials
    
    def initialize(args={})
      @url = Dokker.configuration.url
      @api_key = Dokker.configuration.api_key
      @api_secret = Dokker.configuration.api_secret
    end

    def token(args={}) 
      jwt = create_jwt(args.fetch(:user))
    end

    # def single_sign_on_url(args={}) 
    #   jwt = create_jwt(args.fetch(:user))
    #   query = URI::encode_www_form(integration_code: args[:integration_code], token: jwt)
    #   path = [url, 'sso', @api_key].join("/").concat("?#{query}")
    # end
  
    private
    
    def create_jwt(user)
      token = JWT.encode(claims(user), api_secret, 'HS256')
    end

    def claims(user)
      collected_claims = user.merge(iss: api_key )
      Dokker::Claims.call(collected_claims)
    end
  end
end