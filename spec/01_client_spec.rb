require 'spec_helper'

RSpec.describe Dokker do 

  before(:each) do
    Dokker.configure do |config|
      config.url = "http://app.dokker.test:9280"
      config.api_key = "1fab7eac-48ca-43b5-935e-eff8fa0bb3ae"
      config.api_secret = "e9687a8cec1c46c77534ee20e3e1740c"
    end

    @docker = Dokker::Client.new()
  end
  
  describe "#token" do 
    it "gets the sso url" do 
      token = @docker.token({
        user: { user_id: "xx", email: "jimbo.cortes@go-va.com.au" }, 
      })
      puts token
    end
  end
end