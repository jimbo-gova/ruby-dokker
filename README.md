# Dokker

Ruby client for dokker services. For example: Single-Sign-on Service

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'dokker', '0.1.3', git: 'https://jimbo-gova@bitbucket.org/jimbo-gova/ruby-dokker'
```

And then execute:

    $ bundle

## Usage

### Configuration

```ruby
Dokker.configure do |config|
  config.api_key = "some_api_key"
  config.api_secret = "some_api_secret"
end
```

### Getting the token

```ruby
@docker_client = Dokker::Client.new()
@docker.token(user: { email: "jimbo.cortes@go-va.com.au"})
```

### Adding the element for widget to mount

```html
<div id="dokker-widget" data-api-key="some_api_key" data-jwt="some-token"></div>
```

## ChangeLog

## 0.1.3

- Use token
- Remove url

## 0.1.2

- Modify configuration arguments to `api_key` and `api_secret`.
- Encode using `api_secret`

## 0.1.1

- Separated configuration as a Singleton class.
- Renamed `generate_auth_url` to `single_sign_on_url` with user credentials as a parameter.
- Require to add `integration_code` as a parameter in `single_sign_on_url`. After authentication it will navigate to appropriate integration page.

## 0.1.0

- Pass configuration during initialization of client.
- Implement `generate_auth_url` for single-sign-on.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at `https://jimbo-gova@bitbucket.org/jimbo-gova/ruby-dokker.git`.
